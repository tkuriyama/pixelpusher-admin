
import json
from datetime import datetime
import pandas as pd
import numpy as np
import sys 

pd.options.display.float_format = '{:,.1f}'.format

# IO

def load_log(fname):
    """Load specified log and return as DF."""
    cols = [ 'Timestamp', 'EventType', 'GameID', 'UserCt', 'UserIDs'
           , 'GameMgrEvent', 'UserID'
            , 'ConnEvent', 'ConnUserID', 'ConnToken'
           , 'GameEvent' ]
    df = pd.read_csv(fname, header=None, names=cols)
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])
    return df

# Analysis 

def gen_gamemgr_stats(df):
    """Return DF of player count statistics."""
    df_sub = df[['Timestamp', 'GameMgrEvent', 'UserCt', 'GameID', 'UserID']]
    df_sub = df_sub[df_sub['GameMgrEvent'].isin(['PAdded', 'PRemoved'])]
    
    # Group and reshape to count by Day x Col Values
    cols = [('', 'Date'),
            ('Duration', 'Game_Med'),
            ('Duration', 'User_Med'),
            ('Duration', 'All_User_Sum'),
            ('Uniques', 'GIDs'),
            ('Uniques', 'UIDs'),
            ('# Players', 'PA_Med'), 
            ('# Players', 'PA_Avg'), 
            ('# Players', 'PD_Med'),
            ('# Players', 'PD_Avg')]    
    rows = []
    
    for day, df_g in df_sub.groupby(df['Timestamp'].dt.date):
        gds, uds = game_durs(df_g), user_durs(df_g)
        gpa = df_g[df_g['GameMgrEvent'] == 'PAdded']['UserCt']
        gpd = df_g[df_g['GameMgrEvent'] == 'PRemoved']['UserCt']
        row = [day,
               fmt_td(np.median(gds)), fmt_td(np.median(uds)),
               fmt_td(np.sum(uds)),
               df_g['GameID'].nunique(), df_g['UserID'].nunique(),
               np.median(gpa), gpa.mean(), 
               np.median(gpd), gpd.mean()] 
        rows.append(row)

    return pd.DataFrame(rows, columns=pd.MultiIndex.from_tuples(cols))

def fmt_td(td):
    try:
        if np.isnan(t): return ''
    except:
        s = str(td)
        return s.split('.')[0] if '.' in s else s

def game_durs(df):
    """Return list of all game durations within DF."""
    key, event = 'GameID', 'GameMgrEvent'
    entry, exit = 'GCreated', 'GDestroyed'
    durs = all_durs(df, key, event, entry, exit)
    return compress(durs)

def user_durs(df):
    """Return list of all user durations within DF."""
    key, event = 'UserID', 'GameMgrEvent'
    entry, exit = 'PAdded', 'PRemoved'
    durs = all_durs(df, key, event, entry, exit)
    return compress(durs)

def all_durs(df, key, event, entry, exit):
    """Return list of durations for key column based on entry and exit vals."""
    df_sub = df[['Timestamp', key, event]]
    df_sub = df_sub[df_sub[event].isin([entry, exit])]
    
    rows = df_sub.values.tolist()
    events = []
    d = {}
    for (ts, k, evt) in rows:
        if k not in d and evt == entry:
            d[k] = ts
        elif k in d and evt == exit:
            events.append((k, d[k], ts))
            del d[k]
        # ignore exit events that appear with no entry
        elif k not in d and evt == exit:
            pass
        # on duplicate entry, assume previous exit already occured
        # to be more conservative, just clobber and don't record previous entry
        elif k in d and evt == entry:
            events.append((k, d[k], ts))
            d[k] = ts

    return events

def compress(durs):
    """Take list of (_, entry, exit) and return list of durs in seconds."""
    return [pd.Timestamp(exit, unit='s') - pd.Timestamp(entry, unit='s')
            for _, entry, exit in durs]
       
def gen_gamemgr_cts(df):
    """Return DF of count of Game Mgr events."""
    return single_col_count(df, 'GameMgrEvent')

def gen_conn_cts(df):
    """Return DF of count of Connection events."""
    return single_col_count(df, 'ConnEvent')
            
def gen_game_cts(df):
    """Return DF of count of Game events."""
    return single_col_count(df, 'GameEvent')

def single_col_count(df, col):
    # EventType is only used for row count
    df_sub = df[['Timestamp', 'EventType'] + [col]]
    
    # Group and reshape to count by Day x Col Values
    df_g = df_sub.groupby([df_sub['Timestamp'].dt.date, col]).count()
    df_g = df_g.drop(columns=['Timestamp']).reset_index()
    df_g = df_g.pivot(index='Timestamp',
                      columns=col,
                      values='EventType')
    return df_g

# Aggregation

def gen_stats_str(df):
    """Return string of stats from DF."""
    out = ""

    out += header("Game Manager Statistics")
    out += gen_gamemgr_stats(df).to_string(index=False)
        
    out += header("Game Manager Event Counts")
    out += gen_gamemgr_cts(df).to_string()
        
    out += header("Connection Event Counts")
    out += gen_conn_cts(df).to_string()
    
    out += header("Game Event Counts");
    out += gen_game_cts(df).to_string()

    out += "\n"
    return out

def header(s):
    """Return string formatted as header / separator."""
    sep = "-" * 30
    return '\n\n{} {} {}\n\n'.format(sep, s, sep)

# Main

def main(args):
    """Main"""
    with open('config.json', 'r') as f:
        cfg = json.load(f)

    days = cfg['log']['n_days']
    if not args or len(args) == 1:
        path = cfg['log']['filepath']
    else:
        path = sys.argv[1].strip()

    df = load_log(path)
    df_sub = df[df.Timestamp > datetime.now() -
                pd.to_timedelta("{}day".format(days))]

    if len(df_sub) > 0:
        s = gen_stats_str(df_sub)
        print(s)
    else:
        s = 'No log activity.'
        print('No log activity to in past {} days'.format(days))
        
    return s
        
if __name__ == '__main__':
    main(sys.argv)
