import sys
import json

import log_analysis
import email_lib

def main(args):
    """Daily log gen and email script."""
    if len(args) == 1:
        print('Error: no email config file loaded')
    else:
        with open(args[1], 'r') as f:
            ref = json.load(f)

        body = log_analysis.main(None)
            
        email_lib.send('Pixel Pusher Daily Log',
                       ref['from'],
                       ref['to'],
                       body,
                       ref['server'],
                       ref['auth'])
                       
if __name__ == '__main__':
    main(sys.argv)
